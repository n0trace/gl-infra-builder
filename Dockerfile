# This file is a template, and might need editing before it works on your project. # This Dockerfile installs a compiled binary into a bare system. # You must either commit your compiled binary into source control (not recommended) # or build the binary first as part of a CI/CD pipeline. FROM debian:11 WORKDIR /builder RUN git clone https://gitlab.com/n0trace/gl-infra-builder WORKDIR /builder/gl-infra-builder RUN apt update && apt install binutils bzip2 flex gawk gcc diffutils libc-dev libz-dev make perl python3 python3-pip rsync subversion unzip libncurses5-dev zlib1g-dev gawk gcc-multilib g++-multilib flex git-core gettext libssl-dev ocaml sharutils re2c git -y RUN pip install -r requirements.txt RUN python3 setup.py RUN cd openwrt-19.07/openwrt-19.07.7/ &&\ ./scripts/gen_config.py target_ath79_gl-ar150 glinet_ar150 &&\ make # This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM debian:11 as builder

RUN env
RUN apt update && apt install binutils bzip2 flex gawk gcc diffutils libc-dev libz-dev make perl python python3 python3-pip rsync subversion unzip libncurses5-dev zlib1g-dev gawk gcc-multilib g++-multilib flex git-core gettext libssl-dev ocaml sharutils re2c git wget -y

RUN useradd -m builder
USER builder
WORKDIR /home/builder
RUN  git config --global user.email "you@example.com" &&\
  git config --global user.name "Your Name"
RUN git clone --recursive https://gitlab.com/n0trace/gl-infra-builder

WORKDIR /home/builder/gl-infra-builder
RUN pip install -r requirements.txt
RUN python3 setup.py -c config-21.02.2.yml

WORKDIR /home/builder/gl-infra-builder/openwrt-21.02
RUN ./scripts/gen_config.py target_ath79_6416 glinet_6416 

RUN make -j4

FROM alpine
COPY --from=builder /home/builder/gl-infra-builder/openwrt-21.02/bin /root/openwrt/bin